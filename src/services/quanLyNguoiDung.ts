import { apiInstance } from "constant";
import { LoginSchemaType, RegisterSchemaType } from "schema";
import { User } from "types";

const api = apiInstance({
    baseURL: 'https://movienew.cybersoft.edu.vn/api/QuanLyNguoiDung'
})

export const quanLyNguoiDungServices = {
    register: (payload: RegisterSchemaType) => api.post('/DangKy', payload),
    login: (payload: LoginSchemaType) => api.post<ApiResponse<User>>('/DangNhap', payload)
}