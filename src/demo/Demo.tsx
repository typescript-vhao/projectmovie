export const Demo = () => {
  return (
    <div>
      <h2>Demo Tailwind</h2>
      <p className="text-lg text-[#24b8db] hover:text-red-500 transition-all ease-in-out duration-300">
        Lorem, ipsum dolor sit amet consectetur adipisicing elit. Pariatur,
        quod!
      </p>
    </div>
  );
};
