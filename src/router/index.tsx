import { RouteObject } from "react-router-dom";
import { Demo } from "demo";
import { PATH } from "constant/config";
import { AuthLayout } from "components/layouts";
import { Login, Register } from "pages";

export const router: RouteObject[] = [
  {
    path: "/demo",
    element: <Demo />,
  },
  {
    element: <AuthLayout />,
    children: [
      {
        path: PATH.login,
        element: <Login />,
      },
      {
        path: PATH.register,
        element: <Register />,
      },
    ],
  },
];
